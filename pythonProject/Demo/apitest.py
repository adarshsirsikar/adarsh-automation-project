import requests
from hamcrest import assert_that
import json

response = requests.get("https://github.com/django/django.git")
assert_that(response.status_code, "200", "Comparison Done")

response_json = response.json()
print(response_json)
# Unable to install JSON lib using command "pip install jsonlib"
# jsonlib.c:2161:2: error: implicit declaration of function 'Py_InitModule3' is invalid in C99 [-Werror,-Wimplicit-function-declaration]
            # Py_InitModule3 ("_jsonlib", jsonlib_methods, jsonlib_doc);

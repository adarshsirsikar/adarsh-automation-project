from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome()
driver.get("https://github.com/django")
repo = driver.find_element(By.XPATH, ("/html/body/div[4]/main/div/header/div[2]/nav/div/ul/li[2]/a")).click()
time.sleep(3)
title = driver.title
print(title)
url = driver.current_url
print(url)
repo_names = driver.find_elements(By.XPATH,('//*[@class="d-inline-block"]'))
for repo in repo_names:
    print("REPOSITORY NAME " + repo.text)

repo_desc = driver.find_elements(By.XPATH,('//*[@class="color-fg-muted mb-0 wb-break-word"]'))
for desc in repo_desc:
    print("REPOSITORY DESCRIPTION " + desc.text)

driver.close()